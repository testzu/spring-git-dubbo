package com.jk.model.bootTree;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class BootTree {
    private Integer id;

    private String text;

    private Integer pid;

    private Boolean selectable;

    private List<BootTree> nodes;

    private String href;

}
