package com.jk.model.user;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class User {
    private Integer userId;

    private String userName;

    private Integer userAge;

    private String userInfo;

    private String userHobby;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date userTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date userBirthday;
}
